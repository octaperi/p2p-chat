#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include "datagrama_mensaje.h"
#include "datagrama_saludo.h"
#include <signal.h>

#define GRUPO_MULTICAST "226.1.1.1"

char teclado[1024];
char nombreUsuario[50] = "";
char ipLocal[50] = "";

int socketDatagramaSaludoEnviar;
int socketDatagramaSaludoRecibir;
struct sockaddr_in localSockDatagramaSaludo;

int socketDatagramaMensajeEnviar;
int socketDatagramaMensajeRecibir;
struct sockaddr_in localSockDatagramaMensaje;

pthread_t enviador;
pthread_t recibidorHello;

void construirDatagramaSaludo(struct datagrama_saludo *dataSaludo, char *estado);
void obtenerNombreDeUsuario();
void construirDatagramaMensaje(struct datagrama_mensaje *dataEnviar, char *mensaje);
void construirDatagramaSaludo(struct datagrama_saludo *dataSaludo, char *estado);
void intHandler(int valor);

// Función que se encargará de inicializar el socket para ser utilizado por la aplicación
int inicializarSocket(){

  //Creo el socket sobre el cual voy a enviar el datagrama.
  //dominio = AF_INET significa
  //tipo = SOCK_DGRAM significa
  //protocolo = 0 permite que la función elija el protocolo correcto basado en el tipo
  int sd = socket(AF_INET, SOCK_DGRAM, 0);
  if(sd < 0) {
    perror("Error al abrir el socket");
    exit(1);
  }

  int reuse = 0;
  if(setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, (char *)&reuse, sizeof(reuse)) < 0) {
    perror("Setting SO_REUSEADDR error");
    close(sd);
    exit(1);
  }

  return sd;
}

int inicializarSocketEnviar(struct sockaddr_in localSock, int port) {
  int socket = inicializarSocket();

  // Seteo el grupo multicast a unirme: 226.1.1.1
  // grupo.imr_multiaddr.s_addr = inet_addr(GRUPO_MULTICAST);
  // Seteo la IP de la interfaz mediante la cual recibo los mensajes
  // grupo.imr_interface.s_addr = inet_addr(IP_LOCAL);
  // Bindeo al puerto 4321 especificando que quiero recibir los mensajes de las IPs especificadas en INADDR_ANY
  memset((char *) &localSock, 0, sizeof(localSock));
  localSock.sin_family = AF_INET;
  localSock.sin_addr.s_addr = inet_addr(GRUPO_MULTICAST);
  localSock.sin_port = htons(port);
  if (bind(socket, (struct sockaddr*)&localSock, sizeof(localSock)) < 0) {
    perror("Error al bindear el socket enviar\n");
    close(socket);
    exit(1);
  }

  // Seteo que los mensajes que envío no vuelvan por la interfaz de loopback
  // RECORDAR PONERLO EN 0 CUANDO SE TESTEE CON OTRAS MAQUINAS. AL PONERLO EN 1 HACE QUE LOS MENSAJES DE MULTICAST VUELVAN A ESTA MAQUINA
  int loopch = 0;
  if (setsockopt(socket, IPPROTO_IP, IP_MULTICAST_LOOP, (char *) &loopch, sizeof(loopch)) < 0) {
    perror("setting IP_MULTICAST_LOOP:");
    close(socket);
    exit(1);
  }

  return socket;
}

void inicializarSocketDatagramaMensajeRecibir() {
  socketDatagramaMensajeRecibir = inicializarSocket();

  // Seteo el grupo multicast a unirme: 226.1.1.1
  // grupo.imr_multiaddr.s_addr = inet_addr(GRUPO_MULTICAST);
  // Seteo la IP de la interfaz mediante la cual recibo los mensajes
  // grupo.imr_interface.s_addr = inet_addr(IP_LOCAL);
  // Bindeo al puerto 4321 especificando que quiero recibir los mensajes de las IPs especificadas en INADDR_ANY
  memset((char *) &localSockDatagramaMensaje, 0, sizeof(localSockDatagramaMensaje));
  localSockDatagramaMensaje.sin_family = AF_INET;
  localSockDatagramaMensaje.sin_addr.s_addr = inet_addr(GRUPO_MULTICAST);
  localSockDatagramaMensaje.sin_port = htons(4322);
  if (bind(socketDatagramaMensajeRecibir, (struct sockaddr*)&localSockDatagramaMensaje, sizeof(localSockDatagramaMensaje)) < 0) {
    perror("Error al bindear el socket 4322");
    close(socketDatagramaMensajeRecibir);
    exit(1);
  }


  struct ip_mreq grupoRecibir;
  grupoRecibir.imr_multiaddr.s_addr = inet_addr(GRUPO_MULTICAST);
  grupoRecibir.imr_interface.s_addr = inet_addr(ipLocal);
  if(setsockopt(socketDatagramaMensajeRecibir, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&grupoRecibir, sizeof(grupoRecibir)) < 0) {
    perror("Adding multicast group error");
    close(socketDatagramaMensajeRecibir);
    exit(1);
  }
}

void inicializarSocketDatagramaSaludoRecibir() {
  socketDatagramaSaludoRecibir = inicializarSocket();

  // Seteo el grupo multicast a unirme: 226.1.1.1
  // grupo.imr_multiaddr.s_addr = inet_addr(GRUPO_MULTICAST);
  // Seteo la IP de la interfaz mediante la cual recibo los mensajes
  // grupo.imr_interface.s_addr = inet_addr(IP_LOCAL);
  // Bindeo al puerto 4321 especificando que quiero recibir los mensajes de las IPs especificadas en INADDR_ANY
  memset((char *) &localSockDatagramaSaludo, 0, sizeof(localSockDatagramaSaludo));
  localSockDatagramaSaludo.sin_family = AF_INET;
  localSockDatagramaSaludo.sin_addr.s_addr = inet_addr(GRUPO_MULTICAST);
  localSockDatagramaSaludo.sin_port = htons(4324);
  if (bind(socketDatagramaSaludoRecibir, (struct sockaddr *) &localSockDatagramaSaludo,
           sizeof(localSockDatagramaSaludo)) < 0) {
    perror("Error al bindear el socket 4324");
    close(socketDatagramaSaludoRecibir);
    exit(1);
  }

  struct ip_mreq grupoRecibir;

  grupoRecibir.imr_multiaddr.s_addr = inet_addr(GRUPO_MULTICAST);
  grupoRecibir.imr_interface.s_addr = inet_addr(ipLocal);
  if (setsockopt(socketDatagramaSaludoRecibir, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *) &grupoRecibir,
                 sizeof(grupoRecibir)) < 0) {
    perror("Adding multicast group error");
    close(socketDatagramaSaludoRecibir);
    exit(1);
  }
}

void enviarDatagramaSaludo(char *estado) {
  int datalenSaludo;
  char bufferSaludo[size_datagrama_saludo];
  struct datagrama_saludo *dataSaludo;

  dataSaludo = (struct datagrama_saludo *) bufferSaludo;
  datalenSaludo = sizeof(bufferSaludo);

  construirDatagramaSaludo(dataSaludo, estado);

  if(sendto(socketDatagramaSaludoEnviar, dataSaludo, (size_t) datalenSaludo, 0, (struct sockaddr*)&localSockDatagramaSaludo, sizeof(localSockDatagramaSaludo)) < 0) {
    perror("ERROR AL ENVIAR EL SALUDO");
    exit(1);
  }
}

// Función que se encargará de imprimir los mensajes recibidos
void recibir(){

  char bufferRecibir[size_datagrama_mensaje];
  struct datagrama_mensaje *data;

  data = (struct datagrama_mensaje *) bufferRecibir;

  while(1){
    // Leo desde el socket los mensajes
    if(recv(socketDatagramaMensajeRecibir, bufferRecibir, (size_t) sizeof(bufferRecibir), 0) < 0) {
      perror("Error al leer el datagrama RECIBIR");
      close(socketDatagramaMensajeRecibir);
      exit(1);
    }	else {
      printf("\n%s: %s\n", data-> nombreUsuario, data-> mensaje);
    }
  }
}

void *recibidorDatagramaHello() {
  int datalenRecibir;
  char bufferRecibir[size_datagrama_saludo];
  struct datagrama_saludo *dataRecibir;

  dataRecibir = (struct datagrama_saludo *) bufferRecibir;
  datalenRecibir = sizeof(bufferRecibir);

  while(1){
    // Leo desde el socket los mensajes
    if(recv(socketDatagramaSaludoRecibir, bufferRecibir, (size_t) datalenRecibir, 0) < 0) {
      perror("Error al leer el datagrama RECIBIR");
      close(socketDatagramaSaludoRecibir);
      exit(1);
    }	else {
      printf("%s %s\n", dataRecibir-> nombre_usuario, dataRecibir-> estado);
      if (strcmp(dataRecibir-> estado, "se ha conectado") == 0) {
        enviarDatagramaSaludo("está en la sala");
      }
    }
  }
}

void *enviarMensaje(){
  int datalenEnviar;
  char bufferEnviar[size_datagrama_mensaje];
  struct datagrama_mensaje *dataEnviar;

  dataEnviar = (struct datagrama_mensaje *) bufferEnviar;
  datalenEnviar = sizeof(bufferEnviar);

  // Leo lo escrito en el teclado y lo envío
  while (1) {
    fgets(teclado, sizeof(teclado), stdin);
    if (teclado[0] != '\n') {
      teclado[strlen(teclado) - 1] = '\0';

      construirDatagramaMensaje(dataEnviar, teclado);

      if(sendto(socketDatagramaMensajeEnviar, dataEnviar, (size_t) datalenEnviar, 0, (struct sockaddr*)&localSockDatagramaMensaje, sizeof(localSockDatagramaMensaje)) < 0) {
        perror("ERROR AL ENVIAR EL MENSAJE");
        exit(1);
      }
    }
  }
}

int main(int argc, char *argv[]) {
  strcpy(ipLocal, argv[1]);
  signal(SIGINT, intHandler);

  socketDatagramaMensajeEnviar = inicializarSocketEnviar(localSockDatagramaMensaje, 4321);
  socketDatagramaSaludoEnviar = inicializarSocketEnviar(localSockDatagramaSaludo, 4323);
  inicializarSocketDatagramaMensajeRecibir();
  inicializarSocketDatagramaSaludoRecibir();

  obtenerNombreDeUsuario();

  // Creo un hilo que va a ejecutar recibir()
  pthread_create(&enviador, NULL, enviarMensaje, NULL);
  pthread_create(&recibidorHello, NULL, recibidorDatagramaHello, NULL);
  enviarDatagramaSaludo("se ha conectado");
  //INICIO Sección de código sólo ejecutada por el hilo que envía los mensajes
  recibir();
  return 0;
}

void construirDatagramaMensaje(struct datagrama_mensaje *dataEnviar, char *mensaje) {
  strcpy(dataEnviar-> version_protocolo, "1");
  strcpy(dataEnviar-> privacidad, "0");
  strcpy(dataEnviar-> tipo, "0");
  strcpy(dataEnviar-> origen, ipLocal);
  strcpy(dataEnviar-> destino, GRUPO_MULTICAST);
  strcpy(dataEnviar-> nombreUsuario, nombreUsuario);
  strcpy(dataEnviar-> mensaje , mensaje);
  sprintf(dataEnviar-> tamano_datagrama, "%lu", sizeof(dataEnviar));
}

void construirDatagramaSaludo(struct datagrama_saludo *dataSaludo, char *estado) {
  strcpy(dataSaludo-> nombre_usuario , nombreUsuario);
  strcpy(dataSaludo-> estado, estado);
  strcpy(dataSaludo-> origen, ipLocal);
  strcpy(dataSaludo-> destino, GRUPO_MULTICAST);
  strcpy(dataSaludo-> version_protocolo, "1");
  sprintf(dataSaludo-> tamano_datagrama, "%lu", sizeof(dataSaludo));
}

void intHandler(int valor) {
  enviarDatagramaSaludo("se ha desconectado");
  exit(0);
}

void obtenerNombreDeUsuario() {
  //Repito infinitamente hasta que el nombre de usuario no vacío
  while (strlen(nombreUsuario) == 0) {
    printf("Ingrese su nombre de usuario: ");
    fgets(teclado, sizeof(teclado), stdin);
    if (teclado[0] != '\n') {
      teclado[strlen(teclado) - 1] = '\0';
      strcpy(nombreUsuario, teclado);
    } else {
      printf("No se puede tener un nombre vacío\n");
    }
  }
}
