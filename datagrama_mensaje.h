#ifndef GUARDA_DATAGRAMA_MENSAJE
#define GUARDA_DATAGRAMA_MENSAJE

struct datagrama_mensaje {
    char version_protocolo[4];
    char tamano_datagrama[8];
    char privacidad[2];
    char tipo[2];
    char origen[64];
    char destino[64];
    char nombreUsuario[512];
    char mensaje[1024];

};
#define size_datagrama_mensaje sizeof(struct datagrama_mensaje)

#endif
