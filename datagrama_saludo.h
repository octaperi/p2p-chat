#ifndef GUARDA_DATAGRAMA_SALUDO
#define GUARDA_DATAGRAMA_SALUDO

struct datagrama_saludo {
      char version_protocolo[4];
      char tamano_datagrama[8];
      char estado[64];
      char origen[64];
      char destino[64];
      char nombre_usuario[32];
};
#define size_datagrama_saludo sizeof(struct datagrama_saludo)

#endif
